package controller

import (
	"arvan/model"
	"github.com/gin-gonic/gin"
	"net/http"
)

// Define a struct to bind the JSON data

// CreateUser handles the POST request to create a new user
func CreateUser(c *gin.Context) {
	var userQuota model.UserQuota

	println("testttt")
	// Bind the JSON data to the struct
	if err := c.ShouldBindJSON(&userQuota); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := userQuota.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//todo pass tio kafka or any sequence service

	// Respond with the received user data and request ID
	c.JSON(http.StatusOK, gin.H{
		"message":    "User data received",
		"user_quota": userQuota,
	})
}
