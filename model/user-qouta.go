package model

import (
	"arvan/config"
	"errors"
	"strconv"
	"time"
)

type UserQuota struct {
	UserId string `json:"user_id" binding:"required"`
	Id     string `json:"id" binding:"required"`
}

func (uq *UserQuota) Validate() error {

	u := config.UserQuotaConfig{}
	u.InitUserQuotaConfig()

	err := atomicHandleAll(uq.UserId+uq.Id, "minute"+uq.UserId, "monthly"+uq.UserId, u.MinuteCapacity, u.MonthlyCapacity, u.MinuteTtl, u.MonthlyTtl)
	if err != nil {
		return errors.New("validation fail")
	}

	return nil
}

func atomicHandleAll(key1, minuteKey, monthlyKey string, dailyCapacity, monthlyCapacity int, dailyTtl, monthlyTtl time.Duration) error {
	rdb := config.GetRedisClient()

	// Define the Lua script (same as before)
	luaScript := `
        local key1Exists = redis.call('EXISTS', KEYS[1])
        local dailyValue = tonumber(redis.call('GET', KEYS[2]) or '0')
        local monthlyValue = tonumber(redis.call('GET', KEYS[3]) or '0')

        if key1Exists == 0 and dailyValue < tonumber(ARGV[1]) and monthlyValue < tonumber(ARGV[2]) then
            redis.call('SET', KEYS[1], '1')
            redis.call('INCRBY', KEYS[2], 1)
            redis.call('EXPIRE', KEYS[2], ARGV[3])
            redis.call('INCRBY', KEYS[3], 1)
            redis.call('EXPIRE', KEYS[3], ARGV[4])
            return 1
        end

        return 0
    `

	// Load the Lua script into Redis and get its SHA1 digest
	sha, err := rdb.ScriptLoad(config.Ctx, luaScript).Result()
	if err != nil {
		return err
	}

	// Convert TTLs to seconds
	dailyTtlSeconds := int(dailyTtl.Seconds())
	monthlyTtlSeconds := int(monthlyTtl.Seconds())

	// Execute the Lua script atomically
	result, err := rdb.EvalSha(config.Ctx, sha, []string{key1, minuteKey, monthlyKey}, []string{
		strconv.Itoa(dailyCapacity), strconv.Itoa(monthlyCapacity), strconv.Itoa(dailyTtlSeconds), strconv.Itoa(monthlyTtlSeconds),
	}).Result()
	if err != nil {
		return err
	}

	// Check the type of the result and convert it to a boolean
	success := false
	switch val := result.(type) {
	case int64:
		success = val == 1
	default:
		return errors.New("unexpected result type from Lua script")
	}

	if !success {
		// Handle the case when the script didn't execute successfully
		// (e.g., return an error, log the failure, etc.)
		return errors.New("failed to handle keys atomically")
	}

	return nil
}
