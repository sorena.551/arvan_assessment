package model

import (
	"arvan/config"
	"strconv"
	"sync"
	"testing"
	"time"
)

func TestAtomicHandleAll(t *testing.T) {
	config.InitRedisClient()
	rdb := config.GetRedisClient()

	clearRedis()

	key1 := "key1"
	dailyKey := "daily_key"
	monthlyKey := "monthly_key"
	dailyCapacity := 100
	monthlyCapacity := 1000
	dailyTtl := 24 * time.Hour
	monthlyTtl := 30 * 24 * time.Hour

	// Scenario 1: All keys are new, and capacity limits are not exceeded
	err := atomicHandleAll(key1, dailyKey, monthlyKey, dailyCapacity, monthlyCapacity, dailyTtl, monthlyTtl)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	key1Exists, _ := rdb.Exists(config.Ctx, key1).Result()
	if key1Exists != 1 {
		t.Errorf("Expected key1 to exist, but it doesn't")
	}

	dailyValue, _ := rdb.Get(config.Ctx, dailyKey).Int64()
	if dailyValue != 1 {
		t.Errorf("Expected daily value to be 1, but got %d", dailyValue)
	}

	monthlyValue, _ := rdb.Get(config.Ctx, monthlyKey).Int64()
	if monthlyValue != 1 {
		t.Errorf("Expected monthly value to be 1, but got %d", monthlyValue)
	}

	clearRedis()

	// Scenario 2: key1 already exists
	rdb.Set(config.Ctx, key1, "1", 0)
	err = atomicHandleAll(key1, dailyKey, monthlyKey, dailyCapacity, monthlyCapacity, dailyTtl, monthlyTtl)
	if err == nil {
		t.Errorf("Expected error when key1 exists, but got none")
	}

	clearRedis()

	// Scenario 3: Daily capacity exceeded
	rdb.Set(config.Ctx, dailyKey, strconv.Itoa(dailyCapacity), 0)
	err = atomicHandleAll(key1, dailyKey, monthlyKey, dailyCapacity, monthlyCapacity, dailyTtl, monthlyTtl)
	if err == nil {
		t.Errorf("Expected error when daily capacity exceeded, but got none")
	}

	clearRedis()

	// Scenario 4: Monthly capacity exceeded
	rdb.Set(config.Ctx, monthlyKey, strconv.Itoa(monthlyCapacity), 0)
	err = atomicHandleAll(key1, dailyKey, monthlyKey, dailyCapacity, monthlyCapacity, dailyTtl, monthlyTtl)
	if err == nil {
		t.Errorf("Expected error when monthly capacity exceeded, but got none")
	}

	clearRedis()
	var wg sync.WaitGroup
	numGoroutines := 10
	wg.Add(numGoroutines)

	var successCount int
	for i := 0; i < numGoroutines; i++ {
		go func() {
			defer wg.Done()
			err := atomicHandleAll(key1, dailyKey, monthlyKey, dailyCapacity, monthlyCapacity, dailyTtl, monthlyTtl)
			if err == nil {
				successCount++
			}
		}()
	}

	wg.Wait()

	if successCount != 1 {
		t.Errorf("Expected only one goroutine to succeed, but %d succeeded", successCount)
	}

	clearRedis()
}
func clearRedis() {

	rdb := config.GetRedisClient()

	err := rdb.FlushDB(config.Ctx).Err()

	if err != nil {
		panic(err.Error())
	}
}
