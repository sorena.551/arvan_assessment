package main

import (
	"arvan/config"
	"arvan/controller"
	"github.com/gin-gonic/gin"
)

func main() {
	config.InitRedisClient()

	r := gin.Default()
	r.POST("/user", controller.CreateUser)

	r.Run(":8080")
}
