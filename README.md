# Data Processor Implementation

## System Requirements

- Each data item has a unique identifier, and duplicate data should be avoided as much as possible.
- Each user has a specific quota for the number of requests per minute and the total volume of data sent per month.
- If a user exceeds their quota, they should be prevented from sending new data, and it's not possible to delete data after crossing the quota.
- The time taken from data submission to storage is part of the system's obligations to the user.

## Implementation Details

- No need to implement an authentication system or user management.
- No need to implement the data storage part.
- Data input is assumed to be in the form of requests with popular protocols such as HTTP.
- Each input data has two values: a unique ID and a user ID.
- The amount of user requests does not follow a specific trend (users with many requests and users with few requests use the system).
- Each user's quota is different but fixed.
- Duplicate data prevention takes precedence over the deletion of data sent by the user.
- The data uniqueness guarantee period is limited.
- Multiple instances of the service have been deployed to handle high volumes of requests.

## Technologies Used

- Go programming language
- Gin web framework
- Redis for data validation and quota management
- Docker and docker-compose as deployment tools

## Logic and Implementation

1. **User Quota Validation**:
    - Each user's quota is identified by a `id` stored in Redis.
    - If the `id` doesn't exist, and the user's request capacity for the current minute and month hasn't been reached, the data can be processed by the next service.

2. **Duplicate Data Prevention**:
    - Each `id` is stored in Redis.
    - For each request, the system checks if the key exists. If it does, the request cannot be passed to the next service.

3. **Request Rate Limiting per Minute**:
    - The `user_id` is stored in Redis.
    - For each request, the value associated with the `user_id` is incremented.
    - The initial entry has a 1-minute expiration time.
    - If the capacity is reached within less than 1 minute, the request cannot proceed to the next service.

4. **Request Rate Limiting per Month**:
    - The `user_id` is stored in Redis.
    - For each request, the value associated with the `user_id` is incremented.
    - The initial entry has a 1-month expiration time.
    - If the capacity is reached within less than 1 month, the request cannot proceed to the next service.

5. **Race Condition Handling**:
    - To handle race conditions and ensure atomic operations, a Lua script is used.
    - All validation conditions and logic are implemented in the Lua script and loaded into Redis.
    - If all validations pass, the data is stored and passed to the next service; otherwise, the data is rejected.

6. **Horizontal Scaling**:
    - Multiple instances of the Go web server can use the same Redis instance.
    - If a single Redis instance cannot process all data, a Redis cluster can be configured and attached to multiple instances of the Go service.

7. **Docker Compose**:
    - The project can be run without dependencies using Docker Compose with the command `docker-compose up -d`.

8. **Unit Tests**:
    - Unit tests can be run by executing `go test ./...` in the root directory of the project.

### Load Testing

Load testing can be run using the `wrk` tool with the `random_payload.lua` script:

```sh
wrk -t24 -c1000 -d3s --script=random_payload.lua http://localhost:8080
