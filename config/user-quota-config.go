package config

import (
	"time"
)

type UserQuotaConfig struct {
	MinuteTtl       time.Duration
	MonthlyTtl      time.Duration
	MinuteCapacity  int
	MonthlyCapacity int
}

func (u *UserQuotaConfig) InitUserQuotaConfig() {

	u.MinuteCapacity = 3
	u.MonthlyCapacity = 1000000
	u.MinuteTtl = time.Minute
	u.MonthlyTtl = 30 * 24 * time.Hour // 30 days
}
