package config

import (
	"os"
	"sync"

	"context"
	"github.com/go-redis/redis/v8"
)

type RedisConfig struct {
	HOST string
}

var (
	redisClient *redis.Client
	redisOnce   sync.Once
	Ctx         = context.Background()
)

func (c *RedisConfig) Read() {
	//err := godotenv.Load(".env")
	//if err != nil {
	//	log.Fatalf("Error loading .env file")
	//}
	c.HOST = os.Getenv("REDIS_DB_HOST")
}

func InitRedisClient() {
	var config RedisConfig
	config.Read()

	redisOnce.Do(func() {
		redisClient = redis.NewClient(&redis.Options{
			Addr: config.HOST,
		})
	})
}

func GetRedisClient() *redis.Client {
	return redisClient
}
