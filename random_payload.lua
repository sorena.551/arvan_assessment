math.randomseed(os.time())

request = function()
    local user_id = math.random(1, 1000000)
    local id = math.random(1, 1000000000)

    local body = string.format('{"user_id":"%d", "id":"%d"}', user_id, id)

    return wrk.format("POST", "/user", {["Content-Type"] = "application/json"}, body)
end
